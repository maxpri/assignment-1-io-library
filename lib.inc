section .text
 
global _start

; Принимает код возврата и завершает текущий процесс
exit:
    mov rax, 60
    syscall

; Принимает указатель на нуль-терминированную строку, возвращает её длину
string_length:
    xor rax, rax
    push rdi
    .loop:
        cmp byte [rdi + rax], 0
        je .end
        inc rax
        jmp .loop
    .end:
        pop rdi
        ret

; Принимает указатель на нуль-терминированную строку, выводит её в stdout
print_string:
    push rdi
    call string_length
    mov rdx, rax 
    mov rax, 1
    mov rsi, rdi
    mov rdi, 1
    syscall
    pop rdi
    ret

; Переводит строку (выводит символ с кодом 0xA)
print_newline:
    mov rdi, 10

; Принимает код символа и выводит его в stdout
print_char:  
    push rdi
    mov rax, 1
    mov rdi, 1
    mov rsi, rsp
    mov rdx, 1
    syscall
    pop rdi
    ret

; Выводит беззнаковое 8-байтовое число в десятичном формате 
; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды.
; r8 - divisor
print_uint:
    push rdi
    mov rax ,rdi
    mov r8, 10
    mov rcx, 1
    dec rsp
    mov byte [rsp], 0x00 ; null-terminator
    .loop:
        xor rdx, rdx
        div r8
        add rdx, 0x30
        dec rsp   
        mov [rsp], dl ; push remainder to stack
        inc rcx
        cmp rax, 0 ; if divided number is bigger than 0 - divided one more time
        je .printing_string
        jmp .loop
    .printing_string:
        mov rdi, rsp
        push rcx
        call print_string
        pop rcx
        add rsp, rcx
        pop rdi
        ret

; Выводит знаковое 8-байтовое число в десятичном формате 
print_int:
    test rdi, rdi
    jns .print
    push rdi 
    mov rdi, '-'
    call print_char ; prints '-' char if number is negative
    pop rdi
    neg rdi    
    .print:
        jmp print_uint

; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе
string_equals:
    xor rax, rax     
    .loop:
        mov dl, byte [rdi+rax]   
        cmp byte dl, byte [rsi+rax]    
        jne .not_equals ; if first symbols of two strings aren't equal then end
        inc rax
        cmp dl, 0x00            
        je .equals ; if first string ended then strings are equals
        jmp .loop 
    .not_equals:
        mov rax, 0
        ret
    .equals:
        mov rax, 1
        ret

; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
read_char:
    push 0
    xor rax, rax
    mov rdi, 0
    mov rsi, rsp
    mov rdx, 1
    syscall
    pop rax
    ret

; Принимает: адрес начала буфера, размер буфера
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор

;rdi - buffer's adress; rsi - buffer's size; rcx - symbols counter
read_word:
    xor rcx, rcx
    .loop:
        push rdi
        push rsi  
        push rcx
        
        call read_char

        pop rcx
        pop rsi
        pop rdi

        cmp rax, 0 ;if stdin is finished
        je .end

        cmp rax, 0x9 ; tabulation
        je .check_space_symbols

        cmp rax, 0x10 ; newline char
        je .check_space_symbols

        cmp rax, 0x20 ; space
        je .check_space_symbols

        inc rcx

        cmp rcx, rsi
        je .buffer_overflow

        mov [rdi + rcx - 1], rax 

        jmp .loop

        .check_space_symbols:
            cmp rcx, 0
            je .loop
            jne .end  ; if space symbol is after word then end

        .end:
            mov rax, rdi
            mov rdx, rcx
            ret

        .buffer_overflow:
            xor rax, rax
            ret


; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось
parse_uint:
    xor rdx, rdx
    xor rax, rax
    xor rsi, rsi
    xor r8, r8
    mov rcx, 10
.loop:
    mov sil, [rdi + r8]
    cmp sil, 0x39   ;checking if last digit is between 0 and 9
    jg .end
    sub sil, 0x30
    jl .end
    inc r8
    mul rcx
    add rax, rsi
    jmp .loop
.end:
    mov rdx, r8
    ret





; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был) 
; rdx = 0 если число прочитать не удалось

; r10 - 1 if number is neg, 0 if positive
parse_int:
    xor r10, r10
    xor rsi, rsi
    mov sil, [rdi]
    cmp sil, 0x2D ; check if "-"
    jne .parse_unsigned
    inc r10
    inc rdi

    .parse_unsigned:
        push r10
        call parse_uint
        pop r10
        cmp rdx, 0  ; if not parsed - end with rdx=0
        je .end
        cmp r10, 0  ; if positive - end, otherwise - negate number
        je .end
        neg rax
        inc rdx  ; end, inc rdx because of minus sign
    .end:
        ret


; Принимает указатель на строку, указатель на буфер и длину буфера
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0
string_copy:
    push rdi
    push rsi
    push rdx
    call string_length
    cmp rdx, rax
    jle .fail
    xor rcx, rcx
    .loop:
        mov bl, [rdi+rcx]
        mov [rsi + rcx], bl
        inc rcx
        cmp bl, 0
        je .end
        jne .loop
    .fail:
        xor rax, rax
    .end:
        pop rdx
        pop rsi
        pop rdi
        ret
